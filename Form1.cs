﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EjemploConexionBD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pruebaConexion();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            guardar();
        }

        public void pruebaConexion()
        {
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=empresa;" +
                "Integrated Security=True;";
                SqlConnection cnn = new SqlConnection(connectionString);
            try
            {
                cnn.Open();
                MessageBox.Show("Se estableció la conexión!");
                cnn.Close();
            }catch (Exception ex)
            {
                MessageBox.Show("No se pudo establecer conexion !" + ex.Message);
            }
        }

        public void guardar()
        {
            /*string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=empresa;" +
                "Integrated Security=True;";
            SqlConnection cnn = new SqlConnection(connectionString);*/
            try
            {
                string nombre = txNombre.Text;
                string apellido = txApellido.Text;
                string direccion = txDireccion.Text;
                DateTime fechaNacimiento = dtFecha.Value;
                string telefono = txTelefono.Text;

                PersonaServiceReference.Persona p = new PersonaServiceReference.Persona()
                {
                    Nombre = nombre,
                    Apellido = apellido,
                    Direccion = direccion,
                    FechaNacimiento = fechaNacimiento.Date,
                    Telefono = telefono
                };

                PersonaServiceReference.PersonaWebServiceSoapClient service = new PersonaServiceReference.PersonaWebServiceSoapClient();
                PersonaServiceReference.Response r = service.AgregarPersona(p);

                if (r.statusCode == 200)
                {
                    MessageBox.Show(r.message);
                    listar();
                }
                else
                {
                    MessageBox.Show(r.message);
                }

                /*string query = "INSERT INTO Persona(Nombre, Apellido, Direccion, FechaNacimiento, Telefono)" +
                    "VALUES(@Nombre, @Apellido, @Direccion, @FechaNacimiento, @Telefono)";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                cmd.Parameters.Add("@Nombre", SqlDbType.VarChar, 50).Value = nombre;
                cmd.Parameters.Add("@Apellido", SqlDbType.VarChar, 50).Value = apellido;
                cmd.Parameters.Add("@Direccion", SqlDbType.VarChar, 300).Value = direccion;
                cmd.Parameters.Add("@FechaNacimiento", SqlDbType.Date).Value = fechaNacimiento.Date;
                cmd.Parameters.Add("@Telefono", SqlDbType.VarChar, 10).Value = telefono;

                int resultado = cmd.ExecuteNonQuery();

                if(resultado > 0)
                {
                    MessageBox.Show("Se agregó la información!");
                }
                else
                {
                    MessageBox.Show("No se pudo guardar la información!");
                }

                cnn.Close();
                */
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo guardar la información !" + ex.Message);
            }
        }

        public void listar()
        {
           /* string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=empresa;" +
                "Integrated Security=True;";
            SqlConnection cnn = new SqlConnection(connectionString);*/
            try
            {
                /*string query = "SELECT * FROM Persona";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                SqlDataReader resultado = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(resultado);*/

                PersonaServiceReference.PersonaWebServiceSoapClient service = new PersonaServiceReference.PersonaWebServiceSoapClient();
                PersonaServiceReference.Persona[] r = service.ListarPersonas();

                dataGridView1.DataSource = r;

            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo listar la información !" + ex.Message);
            }
        }        
    }
}
